<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
require './controller/main.php';

final class ContactFormTest extends TestCase
{
    private $main;

    protected function setUp() : void
    {
        $this->main = new main();
    }

    protected function tearDown() : void
    {
        $this->main = NULL;
    }

    public function testGetData()
    {
        $array = [
            [
                'data' =>
                    [ 'fullname' => '',
                        'email' => '',
                        'phone' => '',
                        'message' => ''
                    ],
                'check' => 'Please, fill out fields: fullname, email, message'
            ],
            [
                'data' => [
                    'fullname' => '',
                    'email' => 'admin@index.com',
                    'phone' => '6174173844',
                    'message' => 'test message'
                ],
                'check' => 'Please, fill out fields: fullname'
            ],
            [
                'data' => [
                    'fullname' => 'test name',
                    'email' => 'index.com',
                    'phone' => '6174173844',
                    'message' => 'test message'
                ],
                'check' => 'Please, fill out fields: email'

            ],
            [
                'data' => [
                    'fullname' => 'test name',
                    'email' => '',
                    'phone' => '6174173844',
                    'message' => 'test message'
                ],
                'check' => 'Please, fill out fields: email'
            ],
            [
                'data' => [
                    'fullname' => 'test name',
                    'email' => 'admin@index.com',
                    'phone' => '',
                    'message' => 'test message'
                ],
                'check' => null
            ],
            [
                'data' => [
                    'fullname' => 'test name',
                    'email' => 'admin@index.com',
                    'phone' => '6174173844',
                    'message' => ''
                ],
                'check' => 'Please, fill out fields: message'
            ],
            [
                'data' => [
                    'fullname' => 'test name',
                    'email' => 'admin@index.com',
                    'phone' => '6174173844',
                    'message' => 'test message'
                ],
                'check' => null
            ],
        ];

        foreach ($array as $value) {

            parent::setUp();
            $_POST = $value['data'];

            $result = $this->main->getData();
            $this->assertEquals(true, $result);

            $result = $this->main->validate();
            $this->assertEquals($value['check'], $result);
        }
    }

    public function testConnectDb()
    {
        $result = $this->main->connectDb();
        $this->assertEquals(true, $result);
    }

    public function testCheckDb()
    {
        $result = $this->main->checkDb();
        $this->assertEquals(true, $result);
    }

    public function testCheckTable()
    {
        $result = $this->main->checkTable();
        $this->assertEquals(true, $result);
    }

    public function testSendEmail()
    {
        $result = $this->main->sendEmail();
        $this->assertEquals(true, $result);
    }

    public function testAjax()
    {
        $array = [
            'fullname' => 'test full name',
            'email' => 'admin@index.com',
            'phone' => '6174173844',
            'message' => 'test message'
        ];

        parent::setUp();
        $_POST = $array;

        $result = $this->main->ajax();
        $this->assertEquals(json_encode(['status' => true, 'msg' => 'Your message was successfully sent!']), $result);

        $result = $this->main->deleteLastRow();
        $this->assertEquals(true, $result);

    }

}