<?php

    /* AJAX check  */
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        include $_SERVER['DOCUMENT_ROOT']."/controller/main.php";
        $model = new main();
        header("Content-Type: application/json", true);
        echo $model->ajax();
        exit;
    }

    include "index.html";



