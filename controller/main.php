<?php

class main {

    /**
     * SETINGS
     */
    // Db
    const servername = "localhost";
    const username = "root";
    const password = "password";
    const dbname = "contactForm";
    const tablename = "forms";
    // END

    // SEND EMAIL
    const email = "guy-smiley@example.com";
    /**
     * END
     */

    private $conn;
    private $array;
    private $fullname;
    private $email;
    private $phone;
    private $message;
    private $last_id;

    public function __construct() {

        $this->connectDb();

        // check if Db exist
        $db_exist = $this->checkDb();
        if ($db_exist == false) { $this->createDb(); }

        // check if Table exist
        $table_exist = $this->checkTable();
        if ($table_exist == false) { $this->createTable(); }
    }

    /**
     * @return string
     */
    public function ajax() {

        // validate fields: fullname, email, message
        $this->getData();
        $validate = $this->validate();
        if ($validate) {
            $error[] =  $validate;
        } else {
            // send message
            $send = $this->sendEmail();
            if (empty($send)) { $error[] = 'Email was not send.'; }

            // store data in Db
            $insert = $this->storeDb();
            if ($insert != true) { $error[] = 'Form was not store in Db.'; }
        }
        // display error
        if (isset($error)) { return json_encode(['status' => false ,'msg' => implode(" | ", $error)]); }

        // JSON encode and send back to the server
        return json_encode(['status' => true ,'msg' => 'Your message was successfully sent!']);
    }

    /**
     * Get Post from request
     * @return bool
     */
    public function getData() {

        if (!isset($_POST['fullname']) || !isset($_POST['email']) && !isset($_POST['phone']) && !isset($_POST['message'])) {
            return false;
        }

        $this->array = [
            'fullname' => ['value' => filter_var($_POST['fullname'], FILTER_SANITIZE_STRING), 'req' => true],
            'email' => ['value' => filter_var($_POST['email'], FILTER_VALIDATE_EMAIL), 'req' => true],
            'phone' => ['value' => filter_var($_POST['phone'], FILTER_SANITIZE_STRING), 'req' => false],
            'message' => ['value' => filter_var($_POST['message'], FILTER_SANITIZE_STRING), 'req' => true],
        ];

        return true;
    }

    /**
     * Validate fullname, email, message
     * @return string|null
     */
    public function validate() {

        // check if empty value
        $error = null;
        foreach ($this->array as $key => $val) {
            if (empty($val['value']) && $val['req']) { $error[] = $key; }
            $this->$key = $val['value'];
        }

        // display error if isset
        $display = $error ? 'Please, fill out fields: ' . implode(', ', $error) : null;
        return $display;
    }

    /**
     * @return bool|string
     */
    public function connectDb() {
        // Create connection
        $conn = new mysqli(self::servername, self::username, self::password);
        // Check connection
        if ($conn->connect_error) {
            return "Connection failed: " . $conn->connect_error;
        }
        $this->conn = $conn;

        return true;
    }

    /**
     * Check if Table exist
     * @return bool
     */
    public function checkDb() {
        return mysqli_select_db($this->conn, self::dbname) ? true : false;
    }

    /**
     * sql to create database
     */
    public function createDb() {
        if (!mysqli_select_db($this->conn, self::dbname)) {
            $sql = "CREATE DATABASE " . self::dbname;
            $this->conn->query($sql);
        }
    }

    /**
     * Check if Table exist
     * @return bool
     */
    public function checkTable() {
        return mysqli_query( $this->conn, "DESCRIBE `".self::tablename."`" ) ? true : false;
    }

    /**
     * sql to create table
     */
    public function createTable() {
        // sql to create table
        $sql = "CREATE TABLE " . self::tablename . " (
            id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
            fullname VARCHAR(30) NOT NULL,
            email VARCHAR(30) NOT NULL,
            phone VARCHAR(50),
            message TEXT NOT NULL,
            created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
        )";
        $this->conn->query($sql);
    }

    /**
     * Insert data in Db
     * @return bool|string
     */
    public function storeDb() {
        $data = '"'.$this->fullname.'","'.$this->email.'","'.$this->phone.'","'.$this->message.'","'.date("Y-m-d h:i:s").'"';
        $sql = "INSERT INTO ".self::tablename." (`fullname`, `email`, `phone`, `message`, `created_at`)
                    VALUES (".$data.")";
        if ($this->conn->query($sql) !== TRUE) {
            return "Error: " . $sql . "<br>" . $this->conn->error;
        }

        $this->last_id = mysqli_insert_id($this->conn);

        return true;
    }

    public function deleteLastRow() {
        if ($this->last_id) {
            $sql = "DELETE FROM " . self::tablename . " WHERE id = " . $this->last_id . " LIMIT 1";
            $this->conn->query($sql);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Send e-mail
     * @return bool
     */
    public function sendEmail() {
        // the message
        $msg = "Name: " . $this->fullname .
            "\nEmail: " . $this->email .
            "\nPhone: " . $this->phone .
            "\nMessage: " . $this->message;

        // send email
        $send = mail(self::email,"New form", $msg);
        return $send;
    }

    public function __desconstruct() {
        $this->conn->close();
    }

}

