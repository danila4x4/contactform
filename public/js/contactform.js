/*!
 * Contact form
 */
$(document).ready(function() {
    $("#submitContactForm").click(function () {

        $("#submitContactForm").attr("disabled", true);


        var fullname = $("#fullname").val();
        var email = $("#email").val();
        var phone = $("#phone").val();
        var message = $("#message").val();



        $.ajax({
            type: "POST",
            url: 'public/index.php',
            async: false,
            data: {fullname: fullname, email: email, phone: phone, message: message},
            success: function (data) {
                $("#statusContactForm").html(data['msg']);
                if (data['status'] != true) {
                    $("#submitContactForm").attr("disabled", false);
                }
            }
        });

    });
});


